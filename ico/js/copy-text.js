/* Show Wallets Function */

function showDiv(prefix,chooser)
{
	   for(var i=0;i<chooser.options.length;i++)
	   {
		  var div = document.getElementById(prefix+chooser.options[i].value);
		  div.style.display = 'none';
	   }

	   var selectedOption = (chooser.options[chooser.selectedIndex].value);

	   if(selectedOption == "1")
	   {
		  displayDiv(prefix,"1");
	   }
	   if(selectedOption == "2")
	   {
		  displayDiv(prefix,"2");
	   }
	   if(selectedOption == "3")
	   {
		  displayDiv(prefix,"3");
	   }
	   if(selectedOption == "4")
	   {
		  displayDiv(prefix,"4");
	   }
	   if(selectedOption == "5")
	   {
		  displayDiv(prefix,"5");
	   }
	   if(selectedOption == "6")
	   {
		  displayDiv(prefix,"6");
	   }
	   if(selectedOption == "7")
	   {
		  displayDiv(prefix,"7");
	   }
	   if(selectedOption == "8")
	   {
		  displayDiv(prefix,"8");
	   }
	   if(selectedOption == "9")
	   {
		  displayDiv(prefix,"9");
	   }
	   if(selectedOption == "10")
	   {
		  displayDiv(prefix,"10");
	   }
	   if(selectedOption == "11")
	   {
		  displayDiv(prefix,"11");
	   }
	   if(selectedOption == "12")
	   {
		  displayDiv(prefix,"12");
	   }
	   if(selectedOption == "13")
	   {
		  displayDiv(prefix,"13");
	   }
	   if(selectedOption == "14")
	   {
		  displayDiv(prefix,"14");
	   }
	   if(selectedOption == "15")
	   {
		  displayDiv(prefix,"15");
	   }
	   if(selectedOption == "16")
	   {
		  displayDiv(prefix,"16");
	   }
	   if(selectedOption == "17")
	   {
		  displayDiv(prefix,"17");
	   }
	   if(selectedOption == "18")
	   {
		  displayDiv(prefix,"18");
	   }
	   if(selectedOption == "19")
	   {
		  displayDiv(prefix,"19");
	   }
	   if(selectedOption == "20")
	   {
		  displayDiv(prefix,"20");
	   }
	   if(selectedOption == "21")
	   {
		  displayDiv(prefix,"21");
	   }
	   if(selectedOption == "22")
	   {
		  displayDiv(prefix,"22");
	   }
	   if(selectedOption == "23")
	   {
		  displayDiv(prefix,"23");
	   }
	   if(selectedOption == "24")
	   {
		  displayDiv(prefix,"24");
	   }
	   if(selectedOption == "25")
	   {
		  displayDiv(prefix,"25");
	   }
	   if(selectedOption == "26")
	   {
		  displayDiv(prefix,"26");
	   }
	   if(selectedOption == "27")
	   {
		  displayDiv(prefix,"27");
	   }
	   if(selectedOption == "28")
	   {
		  displayDiv(prefix,"28");
	   }
	   if(selectedOption == "29")
	   {
		  displayDiv(prefix,"29");
	   }
	   if(selectedOption == "30")
	   {
		  displayDiv(prefix,"30");
	   }
	   if(selectedOption == "31")
	   {
		  displayDiv(prefix,"31");
	   }
	   if(selectedOption == "32")
	   {
		  displayDiv(prefix,"32");
	   }
	   if(selectedOption == "33")
	   {
		  displayDiv(prefix,"33");
	   }
	   if(selectedOption == "34")
	   {
		  displayDiv(prefix,"34");
	   }
	   if(selectedOption == "35")
	   {
		  displayDiv(prefix,"35");
	   }
	   if(selectedOption == "36")
	   {
		  displayDiv(prefix,"36");
	   }
	   if(selectedOption == "37")
	   {
		  displayDiv(prefix,"37");
	   }
	   if(selectedOption == "38")
	   {
		  displayDiv(prefix,"38");
	   }
	   if(selectedOption == "39")
	   {
		  displayDiv(prefix,"39");
	   }
	   if(selectedOption == "40")
	   {
		  displayDiv(prefix,"40");
	   }
	   if(selectedOption == "41")
	   {
		  displayDiv(prefix,"41");
	   }
	   if(selectedOption == "42")
	   {
		  displayDiv(prefix,"42");
	   }
	   if(selectedOption == "43")
	   {
		  displayDiv(prefix,"43");
	   }
	   if(selectedOption == "44")
	   {
		  displayDiv(prefix,"44");
	   }
	   if(selectedOption == "45")
	   {
		  displayDiv(prefix,"45");
	   }
	   if(selectedOption == "46")
	   {
		  displayDiv(prefix,"46");
	   }
	   if(selectedOption == "47")
	   {
		  displayDiv(prefix,"47");
	   }
	   if(selectedOption == "48")
	   {
		  displayDiv(prefix,"48");
	   }
}

function displayDiv(prefix,suffix)
{
	   var div = document.getElementById(prefix+suffix);
	   div.style.display = 'block';
}

/* 1 Copy Function Bitcoin */

function copyBitcoin() {
  /* Get the text field */
  var copyText = document.getElementById("bitcoin");

  /* Select the text field */
  copyText.select();

  /* Copy the text inside the text field */
  document.execCommand("Copy");

  /* Alert the copied text */
  alert("Copied the wallet address: " + copyText.value);
}

/* 2 Copy Function ethereum */

function copyEthereum() {
  /* Get the text field */
  var copyText = document.getElementById("ethereum");

  /* Select the text field */
  copyText.select();

  /* Copy the text inside the text field */
  document.execCommand("Copy");

  /* Alert the copied text */
  alert("Copied the wallet address: " + copyText.value);
}

/* 3 Copy Function ethereum classic */

function copyEthereumClassic() {
  /* Get the text field */
  var copyText = document.getElementById("ethereum-classic");

  /* Select the text field */
  copyText.select();

  /* Copy the text inside the text field */
  document.execCommand("Copy");

  /* Alert the copied text */
  alert("Copied the wallet address: " + copyText.value);
}

/* 4 Copy Function litecoin */

function copyLitecoin() {
  /* Get the text field */
  var copyText = document.getElementById("litecoin");

  /* Select the text field */
  copyText.select();

  /* Copy the text inside the text field */
  document.execCommand("Copy");

  /* Alert the copied text */
  alert("Copied the wallet address: " + copyText.value);
}

/* 5 Copy Function dash */

function copyDash() {
  /* Get the text field */
  var copyText = document.getElementById("dash");

  /* Select the text field */
  copyText.select();

  /* Copy the text inside the text field */
  document.execCommand("Copy");

  /* Alert the copied text */
  alert("Copied the wallet address: " + copyText.value);
}

/* 6 Copy Function zcash */

function copyZCash() {
  /* Get the text field */
  var copyText = document.getElementById("zcash");

  /* Select the text field */
  copyText.select();

  /* Copy the text inside the text field */
  document.execCommand("Copy");

  /* Alert the copied text */
  alert("Copied the wallet address: " + copyText.value);
}

/* 7 Copy Function doge */

function copyDoge() {
  /* Get the text field */
  var copyText = document.getElementById("doge");

  /* Select the text field */
  copyText.select();

  /* Copy the text inside the text field */
  document.execCommand("Copy");

  /* Alert the copied text */
  alert("Copied the wallet address: " + copyText.value);
}

/* 8 Copy Function bitcoin cash */

function copyBitcoinCash() {
  /* Get the text field */
  var copyText = document.getElementById("bitcoin-cash");

  /* Select the text field */
  copyText.select();

  /* Copy the text inside the text field */
  document.execCommand("Copy");

  /* Alert the copied text */
  alert("Copied the wallet address: " + copyText.value);
}

/* 9 Copy Function golem */

function copyGolem() {
  /* Get the text field */
  var copyText = document.getElementById("golem");

  /* Select the text field */
  copyText.select();

  /* Copy the text inside the text field */
  document.execCommand("Copy");

  /* Alert the copied text */
  alert("Copied the wallet address: " + copyText.value);
}

/* 10 Copy Function augur */

function copyAugur() {
  /* Get the text field */
  var copyText = document.getElementById("augur");

  /* Select the text field */
  copyText.select();

  /* Copy the text inside the text field */
  document.execCommand("Copy");

  /* Alert the copied text */
  alert("Copied the wallet address: " + copyText.value);
}

/* 11 Copy Function basic attention token */

function copyBAT() {
  /* Get the text field */
  var copyText = document.getElementById("basic-attention-token");

  /* Select the text field */
  copyText.select();

  /* Copy the text inside the text field */
  document.execCommand("Copy");

  /* Alert the copied text */
  alert("Copied the wallet address: " + copyText.value);
}

/* 12 Copy Function gnosis */

function copyGnosis() {
  /* Get the text field */
  var copyText = document.getElementById("gnosis");

  /* Select the text field */
  copyText.select();

  /* Copy the text inside the text field */
  document.execCommand("Copy");

  /* Alert the copied text */
  alert("Copied the wallet address: " + copyText.value);
}

/* 13 Copy Function digix */

function copyDigix() {
  /* Get the text field */
  var copyText = document.getElementById("Digix");

  /* Select the text field */
  copyText.select();

  /* Copy the text inside the text field */
  document.execCommand("Copy");

  /* Alert the copied text */
  alert("Copied the wallet address: " + copyText.value);
}

/* 14 Copy Function iconomi */

function copyIconomi() {
  /* Get the text field */
  var copyText = document.getElementById("iconomi");

  /* Select the text field */
  copyText.select();

  /* Copy the text inside the text field */
  document.execCommand("Copy");

  /* Alert the copied text */
  alert("Copied the wallet address: " + copyText.value);
}

/* 15 Copy Function monaco */

function copyMonaco() {
  /* Get the text field */
  var copyText = document.getElementById("monaco");

  /* Select the text field */
  copyText.select();

  /* Copy the text inside the text field */
  document.execCommand("Copy");

  /* Alert the copied text */
  alert("Copied the wallet address: " + copyText.value);
}

/* 16 Copy Function bancor */

function copyBancor() {
  /* Get the text field */
  var copyText = document.getElementById("bancor");

  /* Select the text field */
  copyText.select();

  /* Copy the text inside the text field */
  document.execCommand("Copy");

  /* Alert the copied text */
  alert("Copied the wallet address: " + copyText.value);
}

/* 17 Copy Function aragon */

function copyAragon() {
  /* Get the text field */
  var copyText = document.getElementById("aragon");

  /* Select the text field */
  copyText.select();

  /* Copy the text inside the text field */
  document.execCommand("Copy");

  /* Alert the copied text */
  alert("Copied the wallet address: " + copyText.value);
}

/* 18 Copy Function token card */

function copyTokenCard() {
  /* Get the text field */
  var copyText = document.getElementById("token-card");

  /* Select the text field */
  copyText.select();

  /* Copy the text inside the text field */
  document.execCommand("Copy");

  /* Alert the copied text */
  alert("Copied the wallet address: " + copyText.value);
}

/* 19 Copy Function civic */

function copyCivic() {
  /* Get the text field */
  var copyText = document.getElementById("civic");

  /* Select the text field */
  copyText.select();

  /* Copy the text inside the text field */
  document.execCommand("Copy");

  /* Alert the copied text */
  alert("Copied the wallet address: " + copyText.value);
}

/* 20 Copy Function stox */

function copyStox() {
  /* Get the text field */
  var copyText = document.getElementById("stox");

  /* Select the text field */
  copyText.select();

  /* Copy the text inside the text field */
  document.execCommand("Copy");

  /* Alert the copied text */
  alert("Copied the wallet address: " + copyText.value);
}

/* 21 Copy Function EOS */

function copyEOS() {
  /* Get the text field */
  var copyText = document.getElementById("eos");

  /* Select the text field */
  copyText.select();

  /* Copy the text inside the text field */
  document.execCommand("Copy");

  /* Alert the copied text */
  alert("Copied the wallet address: " + copyText.value);
}

/* 22 Copy Function first blood */

function copyFirstBlood() {
  /* Get the text field */
  var copyText = document.getElementById("first-blood");

  /* Select the text field */
  copyText.select();

  /* Copy the text inside the text field */
  document.execCommand("Copy");

  /* Alert the copied text */
  alert("Copied the wallet address: " + copyText.value);
}

/* 23 Copy Function status */

function copyStatus() {
  /* Get the text field */
  var copyText = document.getElementById("status");

  /* Select the text field */
  copyText.select();

  /* Copy the text inside the text field */
  document.execCommand("Copy");

  /* Alert the copied text */
  alert("Copied the wallet address: " + copyText.value);
}

/* 24 Copy Function qtum */

function copyQtum() {
  /* Get the text field */
  var copyText = document.getElementById("qtum");

  /* Select the text field */
  copyText.select();

  /* Copy the text inside the text field */
  document.execCommand("Copy");

  /* Alert the copied text */
  alert("Copied the wallet address: " + copyText.value);
}

/* 25 Copy Function wings */

function copyWings() {
  /* Get the text field */
  var copyText = document.getElementById("wings");

  /* Select the text field */
  copyText.select();

  /* Copy the text inside the text field */
  document.execCommand("Copy");

  /* Alert the copied text */
  alert("Copied the wallet address: " + copyText.value);
}

/* 26 Copy Function edgeless */

function copyEdgeless() {
  /* Get the text field */
  var copyText = document.getElementById("edgeless");

  /* Select the text field */
  copyText.select();

  /* Copy the text inside the text field */
  document.execCommand("Copy");

  /* Alert the copied text */
  alert("Copied the wallet address: " + copyText.value);
}

/* 27 Copy Function melon */

function copyMelon() {
  /* Get the text field */
  var copyText = document.getElementById("melon");

  /* Select the text field */
  copyText.select();

  /* Copy the text inside the text field */
  document.execCommand("Copy");

  /* Alert the copied text */
  alert("Copied the wallet address: " + copyText.value);
}

/* 28 Copy Function matchpool */

function copyMatchpool() {
  /* Get the text field */
  var copyText = document.getElementById("matchpool");

  /* Select the text field */
  copyText.select();

  /* Copy the text inside the text field */
  document.execCommand("Copy");

  /* Alert the copied text */
  alert("Copied the wallet address: " + copyText.value);
}

/* 29 Copy Function creditbit */

function copyCreditbit() {
  /* Get the text field */
  var copyText = document.getElementById("creditbit");

  /* Select the text field */
  copyText.select();

  /* Copy the text inside the text field */
  document.execCommand("Copy");

  /* Alert the copied text */
  alert("Copied the wallet address: " + copyText.value);
}

/* 30 Copy Function musiconomi */

function copyMusiconomi() {
  /* Get the text field */
  var copyText = document.getElementById("musiconomi");

  /* Select the text field */
  copyText.select();

  /* Copy the text inside the text field */
  document.execCommand("Copy");

  /* Alert the copied text */
  alert("Copied the wallet address: " + copyText.value);
}

/* 31 Copy Function santiment */

function copySantiment() {
  /* Get the text field */
  var copyText = document.getElementById("santiment");

  /* Select the text field */
  copyText.select();

  /* Copy the text inside the text field */
  document.execCommand("Copy");

  /* Alert the copied text */
  alert("Copied the wallet address: " + copyText.value);
}

/* 32 Copy Function maecenas */

function copyMaecenas() {
  /* Get the text field */
  var copyText = document.getElementById("maecenas");

  /* Select the text field */
  copyText.select();

  /* Copy the text inside the text field */
  document.execCommand("Copy");

  /* Alert the copied text */
  alert("Copied the wallet address: " + copyText.value);
}

/* 33 Copy Function salt */

function copySalt() {
  /* Get the text field */
  var copyText = document.getElementById("salt");

  /* Select the text field */
  copyText.select();

  /* Copy the text inside the text field */
  document.execCommand("Copy");

  /* Alert the copied text */
  alert("Copied the wallet address: " + copyText.value);
}

/* 34 Copy Function tenx */

function copyTenx() {
  /* Get the text field */
  var copyText = document.getElementById("tenx");

  /* Select the text field */
  copyText.select();

  /* Copy the text inside the text field */
  document.execCommand("Copy");

  /* Alert the copied text */
  alert("Copied the wallet address: " + copyText.value);
}

/* 35 Copy Function enjin */

function copyEnjin() {
  /* Get the text field */
  var copyText = document.getElementById("enjin");

  /* Select the text field */
  copyText.select();

  /* Copy the text inside the text field */
  document.execCommand("Copy");

  /* Alert the copied text */
  alert("Copied the wallet address: " + copyText.value);
}

/* 36 Copy Function blockmason */

function copyBlockMason() {
  /* Get the text field */
  var copyText = document.getElementById("blockmason");

  /* Select the text field */
  copyText.select();

  /* Copy the text inside the text field */
  document.execCommand("Copy");

  /* Alert the copied text */
  alert("Copied the wallet address: " + copyText.value);
}

/* 37 Copy Function worldcore */

function copyWorldcore() {
  /* Get the text field */
  var copyText = document.getElementById("worldcore");

  /* Select the text field */
  copyText.select();

  /* Copy the text inside the text field */
  document.execCommand("Copy");

  /* Alert the copied text */
  alert("Copied the wallet address: " + copyText.value);
}

/* 38 Copy Function domraider*/

function copyDOMRaider() {
  /* Get the text field */
  var copyText = document.getElementById("domraider");

  /* Select the text field */
  copyText.select();

  /* Copy the text inside the text field */
  document.execCommand("Copy");

  /* Alert the copied text */
  alert("Copied the wallet address: " + copyText.value);
}

/* 39 Copy Function AION */

function copyAION() {
  /* Get the text field */
  var copyText = document.getElementById("aion");

  /* Select the text field */
  copyText.select();

  /* Copy the text inside the text field */
  document.execCommand("Copy");

  /* Alert the copied text */
  alert("Copied the wallet address: " + copyText.value);
}

/* 40 Copy Function Paypie */

function copyPaypie() {
  /* Get the text field */
  var copyText = document.getElementById("paypie");

  /* Select the text field */
  copyText.select();

  /* Copy the text inside the text field */
  document.execCommand("Copy");

  /* Alert the copied text */
  alert("Copied the wallet address: " + copyText.value);
}

/* 41 Copy Function unikoin */

function copyUnikoin() {
  /* Get the text field */
  var copyText = document.getElementById("unikoin");

  /* Select the text field */
  copyText.select();

  /* Copy the text inside the text field */
  document.execCommand("Copy");

  /* Alert the copied text */
  alert("Copied the wallet address: " + copyText.value);
}

/* 42 Copy Function storm */

function copyStorm() {
  /* Get the text field */
  var copyText = document.getElementById("storm");

  /* Select the text field */
  copyText.select();

  /* Copy the text inside the text field */
  document.execCommand("Copy");

  /* Alert the copied text */
  alert("Copied the wallet address: " + copyText.value);
}

/* 43 Copy Function swarm */

function copySwarm() {
  /* Get the text field */
  var copyText = document.getElementById("swarm");

  /* Select the text field */
  copyText.select();

  /* Copy the text inside the text field */
  document.execCommand("Copy");

  /* Alert the copied text */
  alert("Copied the wallet address: " + copyText.value);
}

/* 44 Copy Function wax */

function copyWax() {
  /* Get the text field */
  var copyText = document.getElementById("wax");

  /* Select the text field */
  copyText.select();

  /* Copy the text inside the text field */
  document.execCommand("Copy");

  /* Alert the copied text */
  alert("Copied the wallet address: " + copyText.value);
}

/* 45 Copy Function presearch */

function copyPresearch() {
  /* Get the text field */
  var copyText = document.getElementById("presearch");

  /* Select the text field */
  copyText.select();

  /* Copy the text inside the text field */
  document.execCommand("Copy");

  /* Alert the copied text */
  alert("Copied the wallet address: " + copyText.value);
}

/* 46 Copy Function viberate */

function copyViberate() {
  /* Get the text field */
  var copyText = document.getElementById("viberate");

  /* Select the text field */
  copyText.select();

  /* Copy the text inside the text field */
  document.execCommand("Copy");

  /* Alert the copied text */
  alert("Copied the wallet address: " + copyText.value);
}

/* 47 Copy Function bitclave */

function copyBitClave() {
  /* Get the text field */
  var copyText = document.getElementById("bitclave");

  /* Select the text field */
  copyText.select();

  /* Copy the text inside the text field */
  document.execCommand("Copy");

  /* Alert the copied text */
  alert("Copied the wallet address: " + copyText.value);
}

/* 48 Copy Function dentacoin */

function copyDentacoin() {
  /* Get the text field */
  var copyText = document.getElementById("dentacoin");

  /* Select the text field */
  copyText.select();

  /* Copy the text inside the text field */
  document.execCommand("Copy");

  /* Alert the copied text */
  alert("Copied the wallet address: " + copyText.value);
}
