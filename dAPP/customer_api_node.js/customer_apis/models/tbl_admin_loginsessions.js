"use strict";

module.exports = function(sequelize, DataTypes) {
  var admin_login = sequelize.define("tbl_admin_loginsessions", {
    fk_admin_id: DataTypes.INTEGER,
    ip_address:DataTypes.STRING,
    browser_session_id:DataTypes.STRING,
    user_agent:DataTypes.STRING,
    login_timestamp:DataTypes.DATE
    
  });

  return admin_login;
};