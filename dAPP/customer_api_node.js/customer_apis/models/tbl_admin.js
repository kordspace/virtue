"use strict";

module.exports = function(sequelize, DataTypes) {
  var Cust = sequelize.define("tbl_admins", {
    login_email: DataTypes.STRING,
    login_pwd:DataTypes.STRING,
    f_name:DataTypes.STRING,
    l_name:DataTypes.STRING,
    has_profile_picture:DataTypes.INTEGER,
    file_extension:DataTypes.STRING,
    is_active:DataTypes.INTEGER,
    last_activity_timestamp:DataTypes.DATE,
    last_login_timestamp:DataTypes.DATE,
    admin_since_timestamp:DataTypes.DATE
    
  });

  /*Cust.associate = function(models) {
    Cust.hasMany(models.tbl_products);
    status:DataTypes.BIGINT(11)
  }*/
  
  return Cust;
};